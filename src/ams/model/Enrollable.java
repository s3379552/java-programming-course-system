package ams.model;

import ams.model.exception.EnrollmentException;

public interface Enrollable {

	public void enrollIntoCourse(Course course) throws EnrollmentException;
	
	public void withdrawFromCourse(Course course) throws EnrollmentException;
}
