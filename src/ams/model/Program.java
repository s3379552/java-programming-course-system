package ams.model;

import java.util.ArrayList;

import ams.model.exception.ProgramException;

public class Program {

	public ArrayList<Course> courses = new ArrayList<Course>();
	private String pCode;
	private String name;
	
	public Program(String pCode, String name){
		this.pCode = pCode;
		this.name = name;
	}
	
	public void addCourse(Course course) throws ProgramException{
		courses.add(course);
	}
	
	public void removeCourse(Course course) throws ProgramException{
		courses.remove(course);
	}
	
	public Course getCourse(String code) {
		//loop through the program's list of courses
		//if a course object matching the input courseID is found, return that course object
		//otherwise, return null
		for(int i = 0; i < courses.size(); i++){
			if(courses.get(i).getCode().equals(code))
			{
				return courses.get(i);
			}
		}
		return null;
	}
	
	public Course[] getAllCourses(){
		//if there are currently no courses in the program, return null
		if(courses.isEmpty()){
			return null;
		}
		//convert the courses ArrayList to an Array type (new object), then return the new Array
		Course courseArray[] = new Course[courses.size()];
		courses.toArray(courseArray);
		return courseArray;
	}
	
	public String toString(){
		return pCode + ":" + name;
	}
}
