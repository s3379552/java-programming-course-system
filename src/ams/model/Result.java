package ams.model;

public class Result {

	private Course course;
	private boolean grade;
	
	
	public Result(Course course, boolean grade){
		this.course = course;
		this.grade = grade;
	}
	
	public Course getCourse(){
		return course;
	}
	
	public String setCourse(Course newCourse){
		return newCourse.getCode();
	}
	
	public boolean getGrade(){
		return grade;
	}
	
	public String setGrade(Boolean newGrade){
		if(newGrade)
			 return "PASS";
		return "FAIL";
	}
	
	public String toString(){
		return getCourse().getCode() + ":" + setGrade(getGrade());
	}
	
	public boolean equals(Object o){
		return super.equals(o);
	}
	
}
