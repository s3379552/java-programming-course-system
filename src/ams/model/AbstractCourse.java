package ams.model;

import java.util.Arrays;


public abstract class AbstractCourse implements Course {

	private String code;
	private String title;
	private int cPoints;
	private String[] preReqs;
	
	public AbstractCourse(String code, String title, int cPoints, String[] preReqs){
		this.code = code;
		this.title = title;
		this.cPoints = cPoints;
		this.preReqs = preReqs;
	}
	public AbstractCourse(String code, String title, String[] preReqs) {
		this.code = code;
		this.title = title;
		this.preReqs = preReqs;
		cPoints = 12;
	}
	@Override
	public String getCode() {
		return code;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public int getCreditPoints() {
		return cPoints;
	}
	
	@Override
	public String convertPreReqs(){
		
		//convert the Array of prerequisites to one long string
		//if the prerequisite array is empty, return null
		StringBuilder sBuilder = new StringBuilder();
		if(getPreReqs() == null)
			return null;
		for(String preReqString : getPreReqs()) {
		    sBuilder.append(preReqString);
		}
		return sBuilder.toString();

	}
	@Override
	public String[] getPreReqs() {
		return preReqs;
	}
	
	public String toString(){
		//if the course has no prerequisites, do not add null to the end of the toString method
		if(getPreReqs() == null){
			return getCode() + ":" + getTitle() + ":" + getCreditPoints();
		}
		return getCode() + ":" + getTitle() + ":" + getCreditPoints() + ":" + convertPreReqs();
	}
	
	public boolean equals(Object o){
		return super.equals(o);
	}

}
