package ams.model.facade;

import java.util.ArrayList;

import ams.model.*;
import ams.model.exception.*;

public class AMSFacade implements AMSModel {
	University university;

	public AMSFacade(){
		university = new University();
		
	}
	// adds new student to university
	public void addStudent(Student newStudent){
		university.setStudent(newStudent);
	}
	
	// returns the (current) student
	public Student getStudent(){
		return university.getStudent();
	}

	// adds new program to university
	public void addProgram(Program program){
		university.setProgram(program);
	}
	
	// returns the (current) program
	public Program getProgram(){
		return university.getProgram();
	}	

	/*- add/remove course to/from the (current) program -*/
	/** throws exception if courses specified as prereqs for newCourse do not exist */
	public void addCourse(Course course) throws ProgramException{
			university.addCourse(course);
	}

	/** throws exception if the course to be removed is a prereq for the other course/s*/
	public void removeCourse(String courseId) throws ProgramException{
		university.removeCourse(courseId);
	}

	// returns a course according to the provided courseCode
	public Course getCourse(String courseCode){
		return university.getCourse(courseCode);
	}

	// returns a collection of all courses in the (current) program
	public Course[] getAllCourses(){
		return university.getAllCourses();
	}
	
	/*- enrol/withdraw student to/from a given course (defined by courseID) -*/
	/** throws exceptions when the enrolment constraints are not maintained
	 * (refer to Ass1 PartB specs)*/
	public void enrollIntoCourse(String courseID) throws EnrollmentException{
		university.enrollStudentIntoCourse(courseID, getStudent());
	}
	
	/** throws exceptions if the student is not currently enrolled into the
	 * course (defined by the courseId) */
	public void withdrawFromCourse(String courseID) throws EnrollmentException{
		university.withdrawFromCourse(courseID, getStudent());
	}

	// adds a new result. returns false if the course associated with this
	// result does not exist in the collection of all currently enrolled
	// courses. Note: you need to remove the associated course from the
	// collection of currently enrolled courses upon entering the result to 
	// indicate that this course is no longer current.
	public boolean addResult(Result result){
		return university.addStudentResult(result, getStudent());
	}

	// returns a collection of all results for the (current) student
	public Result[] getResults(){
		return university.getStudentResult(getStudent());
	}

	// returns a collection of all currently enrolled courses for the student
	public Course[] getCurrentEnrollment(){
		return university.getCurrentStudentEnrollment(getStudent());
	}

	
	@Override
	public int calculateCurrentLoad(){
		return university.calculateCurrentStudentLoad(getStudent());
	}

	@Override
	public int calculateCareerPoints() {
		return university.calculateTotalCareerPoints(getStudent());
	}

}
