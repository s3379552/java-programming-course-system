package ams.model;

public interface Course {
	
	public String getCode();
	
	public String getTitle();
	
	public int getCreditPoints();
	
	public String[] getPreReqs();
	
	public String convertPreReqs();
}
