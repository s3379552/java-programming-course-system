package ams.model;
import java.util.ArrayList;

import ams.model.exception.*;

public class University {
	private Student student;
	private Program program;
	private ArrayList<Course> courses;
	
	public University() {
		student = null;
		program = null;
		courses = new ArrayList<Course>();
	}
	
	public Student getStudent(){
		return student;
	}
	
	public void setStudent(Student s){
		student = s;
		
	}
	
	public Program getProgram(){
		return program;
	}
	
	public void setProgram(Program p){
		program = p;
	}
	
	public void addCourse(Course course) throws ProgramException{
		boolean courseFound;
		
		//if the specified course has to prerequisite courses, then add the course to the university and program
		//skip rest of checks, as they are only for courses with prerequisites
		if(course.getPreReqs() == null){
			courses.add(course);
			program.addCourse(course);
			return;
		}
		
		//loop through the course's prerequisite array
		//check to see if each of the prerequisites already exist in the list of courses
		//if one of them doesn't, throw a ProgramException
		//if they all do, add the course to the program and the university's overall list of courses
		for(int i = 0; i < course.getPreReqs().length; i++){
			courseFound = false;
			String preReqCourse = course.getPreReqs()[i];
			for(int j = 0; j < courses.size(); j++){
				if(courses.get(j).getCode().equals(preReqCourse))
				{
					courseFound = true;
					
				}
			}
			if(!courseFound)
				throw new ProgramException("Pre Requisite Course not found!");
		}
		program.addCourse(course);
		courses.add(course);
	}
	
	public void removeCourse(String courseId) throws ProgramException{
		boolean courseFound = false;
		Course selectedCourse = null;
		String preReqString;
		
		//find the course object linked to the input courseID
		for(int k = 0; k < courses.size(); k++){
			if(courses.get(k).getCode().equals(courseId))
			{
				selectedCourse = courses.get(k);
				courseFound = true;
			}

		}
		
		//if the courseID can't be found in the list of courses, throw a ProgramException
		if (!courseFound)
			throw new ProgramException("Specified course ID (" + courseId + ") not found!");
		
		//if the specified course is currently a prerequisite for other existing courses, throw a ProgramException
		for(int i = 0; i < courses.size(); i++){
			preReqString = courses.get(i).convertPreReqs();
			if (preReqString != null && preReqString.contains(selectedCourse.getCode())){
				throw new ProgramException("Specified course is currently a pre requisite for other courses, so it cannot be currently removed.");
			}
		}
		program.removeCourse(selectedCourse);
		courses.remove(selectedCourse);
	}

	
	public Course getCourse(String courseCode){
		return program.getCourse(courseCode);
	}
	
	public Course[] getAllCourses(){
		//if there are currently no courses in the university, return null
		if(courses.isEmpty()){
			return null;
		}
		
		//convert the courses ArrayList to an Array type (new object), then return the new Array
		Course courseArray[] = new Course[courses.size()];
		courses.toArray(courseArray);
		return courseArray;
	}
	
	public Course[] getProgramCourses(){
		return program.getAllCourses();
	}
	
	public void enrollStudentIntoCourse(String courseID, Student student) throws EnrollmentException{
		Course requestedCourse = null;
		boolean courseFound = false;
		
		//find the course object linked to the input courseID
		for(int i = 0; i < courses.size(); i++){
			if(courses.get(i).getCode().equals(courseID)){
				requestedCourse = courses.get(i);
				courseFound = true;
			}
		}
		
		//if the course object wasn't found, throw an EnrollmentException
		//otherwise, run the student enrollIntoCourse method
		if (!courseFound)
			throw new EnrollmentException("Specified course ID (" + courseID + ") not found");
		
		student.enrollIntoCourse(requestedCourse);
	}
	
	public void withdrawFromCourse(String courseID, Student student) throws EnrollmentException{
		Course requestedCourse = null;
		Boolean courseFound = false;
		
		//find the course object linked to the input courseID
		//if the course wasn't found, throw an EnrollmentException
		//if the course was found, run the student withdrawFromCourse method
		for(int i = 0; i < courses.size(); i++){
			if(courses.get(i).getCode().equals(courseID)){
				requestedCourse = courses.get(i);
				courseFound = true;
				student.withdrawFromCourse(requestedCourse);
				return;
			}
		}
		if(!courseFound){
			throw new EnrollmentException("Specified course ID (" + courseID + ") not found");
		}
	}
	
	public boolean addStudentResult(Result result, Student student){
		return student.addResult(result);
	}
	
	public Result[] getStudentResult(Student student){
		return student.getResults();
	}
	
	public Course[] getCurrentStudentEnrollment(Student student){
		return student.getCurrentEnrollment();
		
	}
	
	public int calculateCurrentStudentLoad(Student student){
		return student.calculateCurrentLoad();
	}
	
	public int calculateTotalCareerPoints(Student student) {
		return student.calculateCareerPoints();
	}
}
