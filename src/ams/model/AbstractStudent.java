package ams.model;
import java.util.ArrayList;

import ams.model.exception.EnrollmentException;

public abstract class AbstractStudent implements Student {

	private String name;
	private int id;
	public ArrayList<Result> results = new ArrayList<Result>();
	public ArrayList<Course> courses = new ArrayList<Course>();
	
	public AbstractStudent(int id, String name){
		this.id = id;
		this.name = name;
	}
	
	
	@Override
	public String getFullName() {
		return name;
	}

	@Override
	public int getStudentId() {
		return id;
	}

	@Override
	public Result[] getResults() {
		
		//if there are currently no results in the results ArrayList, return null
		if(results.isEmpty()){
			return null;
		}
		
		Result nextResult = null;
		Result[] listOfResults = new Result[results.size()];
		
		//convert the results ArrayList to an Array type (new object), then return the new Array
		for(int i = 0; i < results.size(); i++){
			nextResult = results.get(i);
			listOfResults[i] = nextResult;
			nextResult = null;
		}
		return listOfResults;
	}
	
	public ArrayList<Course> getCourses(){
		return courses;
	}
	
	public ArrayList<Result> getResultArray(){
		return results;
	}

	public abstract int getLoad();
	
	
	@Override
	public boolean addResult(Result res) {
		
		//loop through the currently enrolled courses ArrayList
		//if the course related to new result is currently enrolled in (as it should be)
		//withdraw from the course, add the result to the results ArrayList and return true
		//otherwise return false
		for(int i = 0; i < courses.size(); i++){
			if(res.getCourse().equals(courses.get(i))){
				results.add(res);
				courses.remove(i);
				return true;
			}
		}
		return false;
	}

	@Override
	public Course[] getCurrentEnrollment() {
		Course enrolledCourse = null;
		
		//if there are no currently enrolled courses, return null
		if(courses.isEmpty())
			return null;
		
		//convert the results ArrayList to an Array type (new object), then return the new Array
		Course[] enrollment = new Course[courses.size()];
		for(int i = 0; i < courses.size(); i++){
			enrolledCourse = courses.get(i);
			enrollment[i] = enrolledCourse;
			enrolledCourse = null;
		}
		return enrollment;
	}

	@Override
	public int calculateCurrentLoad() {
		int currentLoad = 0;
		
		//loop through the currently enrolled course arrayList
		//for each course in the array, add the course's credit points to the total current load
		//return the total current load (in credit points)
		for(int i = 0; i < getCourses().size(); i++){
			currentLoad += getCourses().get(i).getCreditPoints();
		}
		return currentLoad;
	}

	@Override
	public int calculateCareerPoints() {
		int totalCPoints = 0;
		
		//for each result in the results ArrayList
		//add the related course's credit points to the total career credit points
		//return the total career points (in credit points)
		for(int i = 0; i < results.size(); i++){
			if(results.get(i).getGrade() == true){
				totalCPoints += results.get(i).getCourse().getCreditPoints();
			}
		}
		return totalCPoints;
	}
	
	@Override
	public void enrollIntoCourse(Course course) throws EnrollmentException {
		
		if(getCourses().contains(course)){
			throw new EnrollmentException("Specified course is currently enrolled in by student");
		}
		
		//if the student has no prior results and the course has no pre Reqs, enroll them in the course
				if(getResults() == null && course.getPreReqs() == null){
					courses.add(course);
					return;
				}
		
		//checks to see if there is a fail result for the required course in the student's result array
		//if there is, delete the fail result and enroll them into the course
		for(int i = 0; i < results.size(); i++){
			if(results.get(i).getCourse().equals(course) && results.get(i).getGrade() == false){
				results.remove(i);
				courses.add(course);
				return;
			}
		}
		//checks to see if a pass result for the specified course already exists for the student
		//if it does, throw an EnrollmentException
		for(int i = 0; i < results.size(); i++){
			if(results.get(i).getCourse().equals(course) && results.get(i).getGrade() == true){
					throw new EnrollmentException("Pass grade already found for the specified course");
			}
		}
		courses.add(course);
	}

	@Override
	public void withdrawFromCourse(Course course) throws EnrollmentException {
		Boolean courseFound = false;
		
		//if there are no currently enrolled courses, throw an EnrollmentException
		if(courses.isEmpty())
			throw new EnrollmentException("No courses currently enrolled in to withdraw from");
		
		//if the specified course is found in the list of currently enrolled courses
		//remove that course from the list of currently enrolled courses
		for(int i = 0; i < courses.size(); i++){
			if(courses.get(i).equals(course)){
				courseFound = true;
				courses.remove(course);
				return;
			}
		}
		if(!courseFound){
			throw new EnrollmentException("Specified course has not been enrolled in");
		}
	}
	
	public String toString(){
		return getStudentId() + ":" + getFullName() + ":" + getLoad();
	}

}
