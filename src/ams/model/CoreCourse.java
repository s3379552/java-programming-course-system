package ams.model;

public class CoreCourse extends AbstractCourse {
	
	public CoreCourse(String code, String title, String[] preReqs) {
		super(code, title, preReqs);
	}
	
	public String toString(){
		return super.toString() + ":CORE";
	}

}
