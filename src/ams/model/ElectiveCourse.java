package ams.model;

public class ElectiveCourse extends AbstractCourse {

	public ElectiveCourse(String code, String title, int cPoints, String[] preReqs) {
		super(code, title, cPoints, preReqs);
	}
	
	public String toString(){
		return super.toString() + ":ELECTIVE";
	}

}
