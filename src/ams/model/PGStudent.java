package ams.model;

import ams.model.exception.EnrollmentException;

public class PGStudent extends AbstractStudent {

	private int load;
	public PGStudent(int id, String name) {
		super(id, name);
		load = 48;
		// TODO Auto-generated constructor stub
	}
	
	public int getLoad(){
		return load;
	}
	
	public void enrollIntoCourse(Course course) throws EnrollmentException{
		
		//if the student has no prior failures
		if(!checkForFailures()){
			//if the max load + 6 (54) is less than the expected load (current load + the new course's credit points), throw an EnrollmentException
			if((this.getLoad() + 6) < (this.calculateCurrentLoad() + course.getCreditPoints())){
				throw new EnrollmentException("PG Student can only be overloaded by 6 extra credit points");
			}
		}
		//if the student has prior failures
		//if the max load is less than the expected load (current load + the new course's credit points), throw an EnrollmentException
		else if(this.getLoad() < (this.calculateCurrentLoad() + course.getCreditPoints())){
				throw new EnrollmentException("Enrolling into course would exceed maximum student study load");
			}
		
		//if the course has no prerequisites, call the superclass' enrollment method then end this method here
		if(course.getPreReqs() == null){
			super.enrollIntoCourse(course);
			return;
		}
		
		String preReqID = null;
		boolean courseFound = false;
		
		//for each prerequisite course
		//if a pass result for one of the prerequisite courses is found, call the superclass' enrollment method then end this method here 
		//if at least one prerequisite course hasn't been attempted, throw an EnrollmentException
		//if none of the prerequisite courses haven't been passed, throw an EnrollmentException
		for(int i = 0; i < course.getPreReqs().length; i++){
			preReqID = course.getPreReqs()[i];
			courseFound = false;
			for(int j = 0; j < getResultArray().size(); j++){
				if(getResultArray().get(j).getCourse().getCode().equals(preReqID)){
					courseFound = true;
					if(getResultArray().get(j).getGrade()){
						super.enrollIntoCourse(course);
						return;
					}
				}
			}
			if(!courseFound)
				throw new EnrollmentException("All pre req courses must be attempted");
		}
		throw new EnrollmentException("At least one pre req course must be passed to enrol");
	
}
	
	public boolean checkForFailures(){
		
		//loop through the student's results arrayList
		//if there are any fail (false) results in the ArrayList, return true
		//otherwise, return false
		for(int i = 0; i < results.size(); i++){
			if(!results.get(i).getGrade())
				return true;
		}
		return false;
	}

}
