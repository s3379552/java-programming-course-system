package ams.model;

import ams.model.exception.EnrollmentException;

public class UGStudent extends AbstractStudent {

	private int load;
	public UGStudent(int id, String name) {
		super(id, name);
		load = 60;
	}
	
	public int getLoad(){
		return load;
	}
	
	public void enrollIntoCourse(Course course) throws EnrollmentException {
		String preReqID = null;
		boolean courseFound = false;
		
				//if the max load of the UG student is less than the current load plus the extra load from the new course,
				//throw an EnrollmentException
				if(this.getLoad() < (this.calculateCurrentLoad() + course.getCreditPoints())){
					throw new EnrollmentException("Enrolling into specified course would exceed maximum load");
				}
				
			
		//if the specified course has no prerequisites, call the superclass' method and end this method here
		if(course.getPreReqs() == null){
			super.enrollIntoCourse(course);
			return;
		}
		
		//checks to see if all of the prerequisite courses have pass results in the student's results array
		//if one of the prerequisite results is a fail, throw an EnrollmentException
		//if one of the prerequisite courses hasn't been attempted, throw an EnrollmentException
		//otherwise, call the superclass' method
		for(int i = 0; i < course.getPreReqs().length - 1; i++){
			preReqID = course.getPreReqs()[i];
			courseFound = false;
			for(int j = 0; j < getResultArray().size(); j++){
				if(getResultArray().get(j).getCourse().getCode().equals(preReqID)){
					courseFound = true;
					if(!getResultArray().get(j).getGrade()){
						throw new EnrollmentException("All pre req courses must be passed to enrol in this course");
					}
					break;
				}
			}
			if(!courseFound)
				throw new EnrollmentException("All pre req courses must be completed to enrol in this course");
		}
		super.enrollIntoCourse(course);
	}

}
