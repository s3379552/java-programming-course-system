package ams.model;

import java.util.ArrayList;

public interface Student extends Enrollable {

	public String getFullName();
	
	public int getStudentId();
	
	public Result[] getResults();
	
	public boolean addResult(Result res);
	
	public Course[] getCurrentEnrollment();
	
	public int calculateCurrentLoad();
	
	public int calculateCareerPoints();
}
